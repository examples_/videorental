package step1;

public class Movie {
    public static final int CHILDRENS = 2;
    public static final int REGULAR = 0;
    public static final int NEW_RELEASE = 1;

    private final int DAYS_TWO = 2;
    private final int DAYS_THREE = 3;

    private static final int BASE_FREQUENT_RENTER_POINT = 1;
    private static final int BONUS_FREQUENT_RENTER_POINT = 1;


    private String title;
    private int priceCode;

    public Movie(String title, int priceCode) {
        this.title = title;
        this.priceCode = priceCode;
    }

    public int getPriceCode() {
        return priceCode;
    }

    public void setPriceCode(int arg) {
        priceCode = arg;
    }

    public String getTitle() {
        return title;
    }

    double getAmount(int daysRented) {
        double amount = 0;
        switch (getPriceCode()) {
            case REGULAR:
                amount += 2;
                if (daysRented > DAYS_TWO)
                    amount += (daysRented - DAYS_TWO) * 1.5;
                break;

            case NEW_RELEASE:
                amount += daysRented * 3;
                break;

            case CHILDRENS:
                amount += 1.5;
                if (daysRented > DAYS_THREE)
                    amount += (daysRented - DAYS_THREE) * 1.5;
                break;
        }
        return amount;
    }

    int getFrequentRenterPoints(int daysRented) {
        if ((getPriceCode() == NEW_RELEASE) && daysRented > 1)
            return BASE_FREQUENT_RENTER_POINT + BONUS_FREQUENT_RENTER_POINT;
        return BASE_FREQUENT_RENTER_POINT;
    }
}