package step1;

class Rental {
    private Movie movie;
    private int daysRented;

    public Rental(Movie movie, int daysRented) {
        this.movie = movie;
        this.daysRented = daysRented;
    }

    public int getDaysRented() {
        return daysRented;
    }

    public Movie getMovie() {
        return movie;
    }

    double getAmount() {
        return movie.getAmount(getDaysRented());
    }

    String getStatement() {
        return "\t" + String.valueOf(getAmount()) + "(" + getMovie().getTitle() + ")" + "\n";
    }

    int getFrequentRenterPoints() {
        return movie.getFrequentRenterPoints(getDaysRented());
    }

}