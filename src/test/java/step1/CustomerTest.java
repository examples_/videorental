package step1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CustomerTest {
    public static final int TWO_DAYS = 2;
    public static final int THREE_DAYS = 3;
    public static final int ONE_DAY = 1;
    public static final int FOUR_DAYS = 4;

    Customer customer = new Customer("CUSTOME_NAME");

    @Test
    public void statement() {
        assertEquals("Rental Record for CUSTOME_NAME\n" +
                "Amount owed is 0.0\n" +
                "You earned 0 frequent renter pointers",customer.statement());
    }

    @Test
    public void statement_for_regular_movie_for_less_than_3days() {
        Rental rental = createRegularRentalFor(TWO_DAYS);
        customer.addRental(rental);

        String statement = customer.statement();

        assertEquals("Rental Record for CUSTOME_NAME\n" +
                "\t2.0(NOT_IMPORTANT_MOVIE_TIELE)\n" +
                "Amount owed is 2.0\n" +
                "You earned 1 frequent renter pointers", statement);
    }

    @Test
    public void statement_for_regular_movie_for_more_than_2days() {
        Rental rental = createRegularRentalFor(THREE_DAYS);
        customer.addRental(rental);

        assertEquals("Rental Record for CUSTOME_NAME\n" +
                "\t3.5(NOT_IMPORTANT_MOVIE_TIELE)\n" +
                "Amount owed is 3.5\n" +
                "You earned 1 frequent renter pointers",customer.statement());
    }

    @Test
    public void statement_for_newrelease_movie() {
        Rental rental = aRental("TITLE").priceCode(Movie.NEW_RELEASE).rentFor(ONE_DAY).build();

        customer.addRental(rental);

        assertEquals("Rental Record for CUSTOME_NAME\n" +
                "\t3.0(TITLE)\n" +
                "Amount owed is 3.0\n" +
                "You earned 1 frequent renter pointers",customer.statement());
    }

    @Test
    public void statement_for_newrelease_movie_for_more_than_1_days() {
        Rental rental = aRental("TITLE").priceCode(Movie.NEW_RELEASE).rentFor(TWO_DAYS).build();

        customer.addRental(rental);

        assertEquals("Rental Record for CUSTOME_NAME\n" +
                "\t6.0(TITLE)\n" +
                "Amount owed is 6.0\n" +
                "You earned 2 frequent renter pointers",customer.statement());
    }

    @Test
    public void statement_for_childrens_movie_for_less_than_four_days() {
        Rental rental = aRental("TITLE").priceCode(Movie.CHILDRENS).rentFor(THREE_DAYS).build();

        customer.addRental(rental);

        assertEquals("Rental Record for CUSTOME_NAME\n" +
                "\t1.5(TITLE)\n" +
                "Amount owed is 1.5\n" +
                "You earned 1 frequent renter pointers",customer.statement());
    }

    @Test
    public void statement_for_childrens_movie_for_more_than_three_days() {
        Rental rental = aRental("TITLE").priceCode(Movie.CHILDRENS).rentFor(FOUR_DAYS).build();

        customer.addRental(rental);

        assertEquals("Rental Record for CUSTOME_NAME\n" +
                "\t3.0(TITLE)\n" +
                "Amount owed is 3.0\n" +
                "You earned 1 frequent renter pointers",customer.statement());
    }

    @Test
    public void statement_for_severalMovieRentals() {
        customer.addRental(aRental("REGULAR TITLE 1").priceCode(Movie.REGULAR).rentFor(ONE_DAY).build());
        customer.addRental(aRental("REGULAR TITLE 2").priceCode(Movie.REGULAR).rentFor(FOUR_DAYS).build());
        customer.addRental(aRental("New Release TITLE 1").priceCode(Movie.NEW_RELEASE).rentFor(ONE_DAY).build());
        customer.addRental(aRental("New Release TITLE 2").priceCode(Movie.NEW_RELEASE).rentFor(FOUR_DAYS).build());
        customer.addRental(aRental("Children Title 1").priceCode(Movie.CHILDRENS).rentFor(ONE_DAY).build());
        customer.addRental(aRental("Children Title 2").priceCode(Movie.CHILDRENS).rentFor(FOUR_DAYS).build());

        assertEquals("Rental Record for CUSTOME_NAME\n" +
                "\t2.0(REGULAR TITLE 1)\n" +
                "\t5.0(REGULAR TITLE 2)\n" +
                "\t3.0(New Release TITLE 1)\n" +
                "\t12.0(New Release TITLE 2)\n" +
                "\t1.5(Children Title 1)\n" +
                "\t3.0(Children Title 2)\n" +
                "Amount owed is 26.5\n" +
                "You earned 7 frequent renter pointers",customer.statement());
    }

    class RentalBuilder {
        private int priceCode;
        private int daysRented;
        private String title;

        public RentalBuilder(String title) {
            this.title = title;
        }

        public RentalBuilder priceCode(int priceCode) {
            this.priceCode = priceCode;
            return this;
        }

        public RentalBuilder rentFor(int daysRented) {
            this.daysRented = daysRented;
            return this;
        }

        public Rental build() {
            return new Rental(new Movie(title,priceCode),daysRented);
        }
    }

    private RentalBuilder aRental(String title) {
        return new RentalBuilder(title);
    }

    private Rental createNewReleaseFor(int daysRented) {
        return createRental(Movie.NEW_RELEASE, daysRented);
    }

    private Rental createRegularRentalFor(int daysRented) {
        int priceCode = Movie.REGULAR;
        return createRental(priceCode, daysRented);
    }

    private Rental createRental(int priceCode, int daysRented) {
        String title = "NOT_IMPORTANT_MOVIE_TIELE";
        Movie movie = new Movie(title,priceCode);

        return new Rental(movie,daysRented);
    }


}
